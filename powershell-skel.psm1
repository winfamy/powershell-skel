# Get public and private function definition files.
$public  = @( get-childitem -recurse -path $psscriptroot\src\public -filter "*.ps1" -erroraction silentlycontinue )
$private = @( get-childitem -recurse -path $psscriptroot\src\private -filter "*.ps1" -erroraction silentlycontinue )

# source the files to import their functions
foreach($import in @($public + $private))
{
	try
	{
		. $import.fullname
	}
	catch
	{
		Write-Error -Message "Failed to import function $($import.fullname): $_"
	}
}

## below comments are from whenever I copy pasted it IG, leaving this so that
## i can reference it later for my own use when I eventually and inevitably have 
## to write another powershell module and I use this one for reference 
## - GLP
# Here I might...
# Read in or create an initial config file and variable
# Export Public functions ($Public.BaseName) for WIP modules
# Set variables visible to the module and its functions only
export-modulemember -function $public.basename
